﻿using Storm.ExternalEvent;
using Storm.StardewValley.Event;
using System;

namespace InariWhitebear.BetterRNG
{
    [Mod(Author = "Inari Whitebear", Name = "Better RNG", Version = 1.0d)]
    public class BetterRNG
    {
        [Subscribe]
        public void Initialize(InitializeEvent ev)
        {
            ev.Root.Random = new Random();
        }
    }
}
